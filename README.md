# intern-documentation

#1 Writing Paragraphs of Code

>>> x = 1
>>>print(x)
1
>>> x = x+1
>>> print(x)
2
>>> exit()

This is a good test to make sure that you have Python correctly installed.Note that quit() also works to end the interactive session.

**Reserved Words**

and	except	lambda	with
as	finally	nonlocal	while
assert	false	None	yield
break	for	not	
class	from	or	
continue	global	pass	
def	if	raise	
del	import	return	
elif	in	True	
else	is	try	

It means very specific to Python.

x= 2 – Assignment statement

x = x+2 - Assignment with expression
print(x) – Print function 


Program Steps or Program Flow
•	Like a recipe or installation instructions, a program is a sequence of steps to be done in order.
•	Some steps are conditional- thet may be skipped.
•	Sometimes a step or group of steps is to be repeated.
•	Sometimes we store a set of steps to be used over and over as needed sevreal places throughout the program.


**Sequential Steps**

Program:                 Output:
x = 2                           2
x = x+2                         4
print(x)

When a program is running, it flows from one step to the next.
As programmers, we set up “paths” fort he program to follow.



**Conditional Steps**


Program:                                   Output:

x=5                                            Smaller 
if x<10:                                       Finish
    print(‘Smaller’)
if x>20:
    print(‘Bigger’)

print(‘Finish’)


Repeated Steps

Program:                                    Output:
                        
n =5                                                    5
while n >0:                                             4
    print(n)                                            3
    n = n -1                                            2
print(‘Blastoff!’)                                      1
                                                        Blastoff!

Loops(repeated steps) have iteration variables that change each time through a loop.


#2 Expressions

**Constants**

Fixed values such as numbers,letters, and strings are called “constants” because their value does not change.

Numeric constants are as you expect

String constants use single quotes(‘) or double quotes(“).               >>> print(123)

                                                                             123
                                                                         >>> print(98.6)
                                                                            98.6
                                                                         >>> print(‘Hello world’)
                                                                                 Hello world 
Also, you cannot use reserved words as variable names / identifiers.

Variables

•	A variable is a named place in the memory where a programmer can store data and later retrieve the data using the variable “name”.
    
•	Programmers get to choose the names of the variables.

•	You can change the contents of a variable in a later statement.

x = 12.2
y = 14

**Python Variable Name Rules**

Must start with a letter or underscore _
Must consist of letters, numbers, and underscores
Case Sensitive

Good:     spam           eggs       spam23     _speed
Bad:       23spam      #sign        var.12
Different:  spam     Spam          SPAM 

Hours = 35.0
Rate = 12.5
Pay= hours * rate
print(pay)

Python knows about payroll? Because if I name a variable hours, shouldn't Python know that  means hours? And if I name a variable pay does that mean that Python knows about this? And the answer is, no. Python treats all these three as equal.Python doesn't look at the name of your variables.

**Assignment Statements**

We assign a value to a variable using the assignment statement (=)

An assignment statement consists of an expression on the right-hand side and a variable to store the result.
A variable is a memory location used to store a value (0.6)

X = 3.9 * x * (1 – x)

**Numeric Expressions**

Operator        Operation
+                       Addition
-                       Subtraction
*                       Multiplication
/                       Division
**                      Power
%                       Remainder
   
>>> xx = 2                           >>> jj = 23
>>> xx = xx + 2                      >>> kk = jj% 5
>>> print(xx)                        >>> print(kk)
                                              3

**Order of Evaluation**

When we string operators together Pythın must know which one to do first.This is called “operatör precedence.” 
Which operator “takes precedence “ over the others?

X = 1 + 2 * 3 – 4 / 5 ** 6
Operator Precedence Rules

From highest precedence to lowest precedence 

Paranthesis
Power
Multiplication
Addition
Left to Right

e.g.
>>> x = 1 + 2 ** 3 / 4*5
>>> print(x)
11.0

**What does “Type” Mean?**

In Python variables, literals, and constants have a “type”

Python knows the difference between an integer number and a string

>>> aaa = 2 + 3
>>> print(ddd)
5
>>> ccc = ‘hello’ + ‘there’
>>> print(ccc)
hello there



>>> ccc = ‘hello’ + ‘there’
>>> ccc = ccc + 1

Traceback (most recent call last):  File "<stdin>", line 1, in <module>
TypeError: Can't convert 'int' object to str implicitly.

Python says I am lost.I couldn’t understand what you write.I don’t know how to do.

>>> type(ccc)
<class’str’>
>>> type (‘hello’)
<class ‘str’>
>>> type(1)
<class’int’>
>>>


**Type Matters**

•	Python knows what “type” everything is 
•	Some operations are prohibited
•	You cannot “add 1” to a string
•	We can ask Python what type something is by using the type() function.

>>> eee = 'hello ' + 'there'
>>> eee = eee + 1
Traceback (most recent call last):  File "<stdin>", line 1, in <module>TypeError: Can't convert 'int' object to str implicitly
>>> type(eee)
<class'str'>
>>> type('hello')
<class'str'>
>>> type(1)
<class'int'>
>>> 


**Type Conversions**
When you put an integer and floating point in an expression, the integer is implicitly converted to a float.You can control this with the built-in functions int() and float().

>>> print(float(99) + 100)
199.0
>>> i = 42
>>> type(i)
<class'int'>
>>> f = float(i)
>>> print(f)
42.0
>>> type(f)
<class'float'>
>>> 

**Integer Division**

Integer division produces a floating point result.
>>> print(10 / 2) 
5.0
>>> print(9 / 2) 
4.5
>>> print(99 / 100) 
0.99
>>> print(10.0 / 2.0) 
5.0
>>> print(99.0 / 100.0) 
0.99

**String Conversions**
You can also use int() and float() to convert between strings and integers.You will get an error if the string does not contain numeric characters.

>>> sval = '123'
>>> type(sval)
<class 'str'>
>>> print(sval + 1)
Traceback (most recent call last):  File "<stdin>", line 1, in <module>
TypeError: Can't convert 'int' object to str implicitly.

Because’123’ is a string and 1 is a number.So, it is not matching.
>>> ival = int(sval)
>>> type(ival)
<class 'int'>
>>> print(ival + 1)
124
>>> nsv = 'hello bob'
>>> niv = int(nsv)
Traceback (most recent call last):  File "<stdin>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'x'


**User Input**

•	We can instruct Python to pause and read data from the user using the input()  function
•	The input()  function returns a string


nam = input('Who are you? ')
print('Welcome', nam)

Who are you? Chuck
Welcome Chuck
Converting User Input

If we want to read a number from the user, we must convert it from a string to a number using a type conversion function.Later we will deal with bad input data

inp = input('Europe floor?')
usf = int(inp) + 1
print('US floor', usf)

Europe floor? 0
US floor 1

**Comments in Python**
Anything after a # is ignored by Python.# is a comment sign.

Ex:
# Get the name of the file and open it
name = input('Enter file:')
handle = open(name, 'r')
# Count word frequency
counts = dict()
for line in handle:
    words = line.split()
    for word in words:
        counts[word] = counts.get(word,0) + 1
# Find the most common word
bigcount = None
bigword = None
for word,count in counts.items():
    if bigcount is None or count > bigcount:
        bigword = word
        bigcount = count
# All done
print(bigword, bigcount)






